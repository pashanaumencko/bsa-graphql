import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput) {
    messages(orderBy: $orderBy) {
        id
        text
        likes
        dislikes
        replies {
            id
            text
        }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation MessageMutation($text: String!) {
    postMessage(text: $text) {
      id
      text  
      replies {
        id
        text
      }
    }
  }
`;

export const POST_LIKE_MUTATION = gql`
  mutation LikeMutation($id: ID!, $likes: Int!) {
    updateLike(id: $id, likes: $likes) {
        id
        likes
    }
  }
`;

export const POST_DISLIKE_MUTATION = gql`
  mutation DislikeMutation($id: ID!, $dislikes: Int!) {
    updateDislike(id: $id, dislikes: $dislikes) {
        id
        dislikes
    }
  }
`;

export const POST_REPLY_MUTATION = gql`
  mutation ReplyMutation($id: ID!, $text: String!) {
    postReply(id: $id, text: $text) {
      id
      text
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      likes
      dislikes
      replies {
        id
        text
      }
    }
  }
`;