import React from 'react';
import MessageList from '../components/MessageList/MessageList';
import MessageInput from '../components/MessageInput/MessageInput';


import './Chat.css';

const Сhat = (props) => {
  return(
    <div className="container">
      <div className="chat">
        <MessageList />
        <MessageInput />
      </div>
    </div>
  );
}

export default Сhat;

