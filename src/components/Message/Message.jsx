import React from 'react';
import { Mutation } from 'react-apollo';
import { POST_LIKE_MUTATION, POST_DISLIKE_MUTATION, POST_REPLY_MUTATION } from '../../queries';

import './Message.css';


class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReply: false,
      replyText: ''
    }

    this.onReplyClick = this.onReplyClick.bind(this);
    this.onReplyCancel = this.onReplyCancel.bind(this);
  }

  onReplyClick() {
    this.setState({ isReply: true });
  }

  onReplyCancel() {
    this.setState({ isReply: false });
  }

  replyForm(id) {

    return (
      <form>
        <div className="form-group">
          <textarea className="form-control mb-1" onChange={event => this.setState({ replyText: event.target.value })} name="editMessage"></textarea>
          <Mutation
            mutation={POST_REPLY_MUTATION}
            variables={{ id, text: this.state.replyText }}
          >
            {postMutation =>
              <button type="button" onClick={() => {
                this.onReplyCancel();
                postMutation();
              }} className="btn btn-primary mr-3">Reply</button>
            }
          </Mutation>
          <button type="button" onClick={this.onReplyCancel} className="btn btn-outline-primary">Cancel</button>
        </div>
      </form>
    );
  }

  getReplyList(replies) {
    return replies.map(reply => {
      const { id, text } = reply;
      const uniqueReplyId = id.slice(-4);
      return (
        <div className="incoming-msg mb-2">
          <div className="received-msg">
            <div className="reply-withd-msg">
              <p>
                <h6>#{uniqueReplyId}</h6>
                {text}
              </p>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    let { id, text, likes, dislikes, replies } = this.props;
    const uniqueMessageId = id.slice(-4);
    return (
      <div className="incoming-msg">
        <div className="received-msg">

          <div className="received-withd-msg">
            <p>
              <h6>#{uniqueMessageId}</h6>
              {text}
              {this.state.isReply ? this.replyForm(id) : null}
            </p>
            <div className="msg-info">
              <Mutation
                mutation={POST_LIKE_MUTATION}
                variables={{ id, likes }}
              >
                {postMutation => {
                  return <div onClick={postMutation} className="msg-likes mr-3">{likes}<i className="fa fa-thumbs-up"></i></div>;
                }}
              </Mutation>

              <Mutation
                mutation={POST_DISLIKE_MUTATION}
                variables={{ id, dislikes }}
              >
                {postMutation => {
                  return <div onClick={postMutation} className="msg-dislikes mr-3">{dislikes}<i className="fa fa-thumbs-down"></i></div>;
                }}
              </Mutation>
              <div className="msg-reply" onClick={this.onReplyClick}>
                <i className="fa fa-edit"></i>
              </div>
            </div>
            {this.getReplyList(replies)}
          </div>
        </div>
      </div>
    );
  }
}

export default Message;