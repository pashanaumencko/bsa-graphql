import React from 'react';
import Message from './../Message/Message';
import { Query } from 'react-apollo';
import { MESSAGE_QUERY, NEW_MESSAGE_SUBSCRIPTION } from '../../queries';
import Spinner from '../Spinner/Spinner';

import './MessageList.css';

const MessageList = (props) => {
  const orderBy = 'createdAt_DESC';

  const _subscribeToNewMessages = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.find(({ id }) => id === newMessage.id);
        if (exists) return prev;

        return {
          ...prev, 
          messages: [
            newMessage, 
            ...prev.messages
          ],
          __typename: prev.messages.__typename, 
        };
      }
    });
  };

  return (
    <Query query={MESSAGE_QUERY} variables={{ orderBy }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <Spinner />;
        if (error) return <div>Fetch error</div>;
        _subscribeToNewMessages(subscribeToMore);

        const { messages } = data;

        return (
          <div className="message-list">
            {messages.map(message => {
              return <Message key={message.id} {...message} />
            })}
          </div>
        );
      }}
    </Query>
  );
}

export default MessageList;