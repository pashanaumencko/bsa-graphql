import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from '../../queries';

import './MessageInput.css';


const MessageInput = (props) => {
  const [text, setText] = useState('');

  const _updateStoreAfterAddingMessage = (store, newMessage) => {
    const orderBy = 'createdAt_DESC';
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy
      }
    });
    data.messages.unshift(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data,
    });
  };

  return(
    <div className="type-msg">
      <form className="input-msg-write">
        <textarea type="text" onChange={event => setText(event.target.value)}  className="write-msg" name="messageInput" placeholder="Type a message" />

        <Mutation
          mutation={POST_MESSAGE_MUTATION}
          variables={{ text, likes: 0, dislikes: 0 }}
          update={(store, { data: { postMessage } }) => {
            _updateStoreAfterAddingMessage(store, postMessage);
          }}
        >
          {postMutation =>
            <button className="msg-send-btn" onClick={postMutation} type="button">
              <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
            </button>
          }
        </Mutation>
      </form>
    </div>
  );
}

export default MessageInput;