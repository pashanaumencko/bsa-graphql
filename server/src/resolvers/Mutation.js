
function postMessage(parent, args, context, info) {
    return context.prisma.createMessage({
      text: args.text,
    });
}
  
function updateLike(parent, args, context, info) {
    return context.prisma.updateMessage({
        data: {likes: ++args.likes}, 
        where: {id: args.id}
    });
}

function updateDislike(parent, args, context, info) {
    return context.prisma.updateMessage({
        data: {dislikes: ++args.dislikes}, 
        where: {id: args.id}
    });
}

async function postReply(parent, args, context, info) {
    const messageExists = await context.prisma.$exists.message({
        id: args.id
    });

    if (!messageExists) {
        throw new Error(`message with ID ${args.id} does not exist`);
    }

    return context.prisma.createReply({
        text: args.text,
        message: { connect: { id: args.id } }
    });
}
  
module.exports = {
    postMessage,
    updateLike,
    updateDislike,
    postReply
}